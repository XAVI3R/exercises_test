## Exercice 2 App

Front-end of a simple app displaying a list of posts with the possibility to add post.

* React
* Redux
* SASS
* Bootstrap

### Install

```
yarn install
```

### Usage

```
yarn start
```