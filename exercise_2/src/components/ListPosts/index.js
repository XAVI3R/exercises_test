import React, { Component } from 'react';
import PropTypes from 'prop-types';
import HeadTitle from '../HeadTitle';
import Post from '../Post';
import './listPosts.css';

/**
 * Posts list.
 *
 * @export
 * @class ListPosts
 * @extends {Component}
 */
export default class ListPosts extends Component {
    /**
     * Props validation for the ListPosts component.
     *
     * @static
     * @memberof ListPosts
     */
    static propTypes = {
        listPosts: PropTypes.array.isRequired
    }

    /**
     * Renders the ListPosts component.
     *
     * @returns {ReactElement}
     * @memberof ListPosts
     */
    render () {
        return (
            <div className="listPostsComponent">
                {
                    this.props.listPosts.length > 0 &&
                        <div>
                            <HeadTitle title="List of your posts" />

                            {
                                this.props.listPosts.map((post) => (
                                    <Post
                                        key={post.id}
                                        post={post} />
                                ))
                            }
                        </div>
                }
            </div>
        );
    }
}
