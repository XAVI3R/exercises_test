import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Alert from '../Alert';
import './textarea.css';

/**
 * Textarea.
 *
 * @export
 * @class Textarea
 * @extends {Component}
 */
export default class Textarea extends Component {
    /**
     * Props validation for the Textarea component.
     *
     * @static
     * @memberof Textarea
     */
    static propTypes = {
        handleOnChangeTextarea: PropTypes.func.isRequired,
        content: PropTypes.string.isRequired,
        error: PropTypes.bool.isRequired,
        errorText: PropTypes.array.isRequired,
        rows: PropTypes.string,
        required: PropTypes.bool
    }

    /**
     * Renders the Textarea component.
     *
     * @returns {ReactElement}
     * @memberof Textarea
     */
    render () {
        return (
            <div className="textareaComponent form-group">
                <textarea
                    className="form-control"
                    value={this.props.content}
                    rows={this.props.rows || '3'}
                    required={this.props.required || false}
                    onChange={this.props.handleOnChangeTextarea} />

                {
                    this.props.error &&
                        <Alert
                            classes="alert-warning"
                            text={this.props.errorText} />
                }
            </div>
        );
    }
}
