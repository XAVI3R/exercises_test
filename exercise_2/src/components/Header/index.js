import React, { Component } from 'react';
import logo from '../../style/images/logo.svg';
import './header.css';

/**
 * Header.
 *
 * @export
 * @class Header
 * @extends {Component}
 */
export default class Header extends Component {
    /**
     * Renders the Header component.
     *
     * @returns {ReactElement}
     * @memberof App
     */
    render () {
        return (
            <header className="headerComponent">
                <img src={logo} className="logo" alt="logo" />
                <h1 className="title">Exercise 2 App</h1>
            </header>
        );
    }
}
