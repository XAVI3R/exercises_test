import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './headTitle.css';

/**
 * HeadTitle.
 *
 * @export
 * @class HeadTitle
 * @extends {Component}
 */
export default class HeadTitle extends Component {
    /**
     * Props validation for the HeadTitle component.
     *
     * @static
     * @memberof HeadTitle
     */
    static propTypes = {
        title: PropTypes.string.isRequired
    }

    /**
     * Renders the HeadTitle component.
     *
     * @returns {ReactElement}
     * @memberof HeadTitle
     */
    render () {
        return (
            <div className="headTitleComponent d-flex align-items-center p-3 my-3 text-white-50 rounded box-shadow">
                <div className="lh-100">
                    <h2 className="mb-0 ml-2 text-white lh-100">{this.props.title}</h2>
                </div>
            </div>
        );
    }
}
