import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Loader from '../Loader';

/**
 * Button.
 *
 * @export
 * @class Button
 * @extends {Component}
 */
export default class Button extends Component {
    /**
     * Props validation for the Button component.
     *
     * @static
     * @memberof Button
     */
    static propTypes = {
        text: PropTypes.string.isRequired,
        action: PropTypes.func.isRequired,
        fetching: PropTypes.bool,
        classes: PropTypes.string,
        type: PropTypes.string
    }

    /**
     * Renders the Button component.
     *
     * @returns {ReactElement}
     * @memberof Button
     */
    render () {
        return (
            <button
                type={`${this.props.type || 'button'}`}
                className={`${this.props.classes} btn btn-primary`}
                onClick={this.props.action}>
                {
                    this.props.fetching ?
                        <div className="d-flex">
                            <Loader classes="loaderSmall loaderWhite" />
                            <div className="pl-3">Loading</div>
                        </div> :
                        this.props.text
                }
            </button>
        );
    }
}
