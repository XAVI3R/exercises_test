import React, { Component } from 'react';
import PropTypes from 'prop-types';
import HeadTitle from '../HeadTitle';
import Textarea from '../Textarea';
import Button from '../Button';
import Alert from '../Alert';
import './form.css';

/**
 * Form to create a new post.
 *
 * @export
 * @class Form
 * @extends {Component}
 */
export default class Form extends Component {
    /**
     * Props validation for the Form component.
     *
     * @static
     * @memberof Form
     */
    static propTypes = {
        submitPost: PropTypes.func.isRequired,
        listPosts: PropTypes.array.isRequired,
        fetchingPosts: PropTypes.bool.isRequired,
        errorSavingPost: PropTypes.array
    }

    /**
     * Creates an instance of the Form component.
     *
     * @param {object} props
     * @memberof Form
     */
    constructor (props) {
        super(props);

        this.state = {
            display: false,
            textarea: {
                content: '',
                error: false,
                errorText: [
                    'You need to write some content'
                ]
            },
            errorSavingPost: this.props.errorSavingPost
        };
    }

    /**
     * Handle display of form and error saving and reset value.
     *
     * @param {object} props
     * @param {object} state
     * @memberof Form
     */
    static getDerivedStateFromProps (props, state) {
        if (!props.fetchingPosts) {
            if (!props.errorSavingPost) {
                const textarea = state.textarea;

                textarea.content = '';
                textarea.error = false;

                return {
                    display: false,
                    errorSavingPost: props.errorSavingPost,
                    textarea
                };
            }

            return {
                textarea: {
                    ...state.textarea,
                    error: false
                },
                errorSavingPost: props.errorSavingPost
            };
        }

        return null;
    }

    /**
     * Show the form instead of the button.
     */
    showFormPost = () => {
        this.setState({
            display: true
        });
    }

    /**
     * Handle the change of content for the textarea.
     *
     * @param {Event} event
     * @memberof Form
     */
    handleOnChangeTextarea = (event) => {
        event.persist();

        this.setState(prevState => ({
            textarea: {
                ...prevState.textarea,
                content: event.target.value
            }
        }));
    }

    /**
     * Handle the submit of the post.
     *
     * @memberof Form
     */
    handleSubmit = () => {
        // Test if there is a text to submit
        if (this.state.textarea.content.trim()) {
            const newPost = {
                content: this.state.textarea.content.trim()
            };

            this.props.submitPost(newPost);
        } else {
            this.setState(prevState => ({
                textarea: {
                    ...prevState.textarea,
                    error: true
                },
                errorSavingPost: null
            }));
        }
    }

    /**
     * Renders the Form component.
     *
     * @returns {ReactElement}
     * @memberof Form
     */
    render () {
        return (
            this.state.display ?
                <form className="formComponent">
                    <HeadTitle title="New post" />
                    <Textarea
                        required
                        handleOnChangeTextarea={this.handleOnChangeTextarea}
                        {...this.state.textarea} />

                    {
                        this.state.errorSavingPost &&
                            <Alert
                                classes="alert-danger"
                                text={this.state.errorSavingPost} />
                    }

                    <Button
                        text="Submit"
                        action={this.handleSubmit}
                        fetching={this.props.fetchingPosts}
                        classes="buttonSavePost" />
                </form> :
                <div className={`formComponent ${this.props.listPosts.length > 0 ? 'buttonTop' : 'buttonCenter'}`}>
                    <Button
                        text="Add post"
                        action={this.showFormPost} />
                </div>
        );
    }
}
