import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './alert.css';

/**
 * Alert.
 *
 * @export
 * @class Alert
 * @extends {Component}
 */
export default class Alert extends Component {
    /**
     * Props validation for the Alert component.
     *
     * @static
     * @memberof Alert
     */
    static propTypes = {
        classes: PropTypes.string.isRequired,
        text: PropTypes.array.isRequired,
        buttonAction: PropTypes.func,
        buttonText: PropTypes.string
    }

    /**
     * Renders the Alert component.
     *
     * @returns {ReactElement}
     * @memberof Alert
     */
    render () {
        const hasButton = this.props.buttonAction && this.props.buttonText;

        return (
            <div
                className={`alertComponent alert ${this.props.classes} ${hasButton ? 'alertButton' : ''}`}
                role="alert">
                {
                    hasButton ?
                        [
                            <span className="mt-2" key={0}>{this.props.text}</span>,
                            <a
                                className="btn btn-primary"
                                onClick={this.props.buttonAction}
                                key={1}>
                                {this.props.buttonText}
                            </a>
                        ] :
                        this.props.text.map(content => {
                            return content;
                        })
                }
            </div>
        );
    }
}
