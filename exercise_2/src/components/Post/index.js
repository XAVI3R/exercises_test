import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './post.css';

/**
 * Post.
 *
 * @export
 * @class Post
 * @extends {Component}
 */
export default class Post extends Component {
    /**
     * Props validation for the Post component.
     *
     * @static
     * @memberof ListPosts
     */
    static propTypes = {
        post: PropTypes.object.isRequired
    }

    /**
     * Renders the Post component.
     *
     * @returns {ReactElement}
     * @memberof Post
     */
    render () {
        const commentsNumber = this.props.post.comments.length;

        return (
            <div className="postComponent d-flex text-muted pt-3">
                <p className="content pb-3 mb-0 lh-125 border-bottom border-gray">
                    {
                        commentsNumber > 0 &&
                            <strong className="d-block text-gray-dark">
                                {`${commentsNumber} comment${commentsNumber > 1 ? 's' : ''}`}
                            </strong>
                    }
                    {this.props.post.content}
                </p>
            </div>
        );
    }
}
