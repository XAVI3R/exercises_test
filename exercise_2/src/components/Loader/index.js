import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './loader.css';

/**
 * Loader.
 *
 * @export
 * @class Loader
 * @extends {Component}
 */
export default class Loader extends Component {
    /**
     * Props validation for the Loader component.
     *
     * @static
     * @memberof Loader
     */
    static propTypes = {
        classes: PropTypes.string.isRequired
    }

    /**
     * Renders the Loader component.
     *
     * @returns {ReactElement}
     * @memberof Loader
     */
    render () {
        return (
            <div className={`loaderComponent ${this.props.classes}`} />
        );
    }
}
