import Api from './Api';
import { makeCancelable } from '../helper/Promise';

/**
 * Get the list of posts.
 *
 * @returns {array}
 */
export function getPosts () {
    return makeCancelable(Api.get('/posts')).promise;
}

/**
 * Save an post.
 */
export function savePost (newPost) {
    return makeCancelable(Api.post(`/posts`, newPost)).promise;
}
