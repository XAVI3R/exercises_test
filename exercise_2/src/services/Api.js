import request from 'superagent';

/**
 * Api.
 *
 * @export
 * @class Api
 */
export default class Api {
    /**
     * Set the header of all requests with the access token.
     *
     * @static
     * @param {object} [headers={}]
     * @returns {object}
     * @memberof Api
     */
    static headers (headers = {}) {
        return Object.assign(
            {
                "Cache-Control": "no-cache",
                "Pragma": "no-cache",
                "Expires": "-1"
            }, headers);
    }

    /**
     * Send GET http request.
     *
     * @static
     * @param {string} url
     * @param {object} [headers={}]
     * @returns {Promise}
     * @memberof Api
     */
    static get (url, headers = {}) {
        return request
            .get(url)
            .set(Api.headers(headers));
    }

    /**
     * Send POST http request.
     *
     * @static
     * @param {string} url
     * @param {object} params
     * @param {object} [headers={}]
     * @returns {Promise}
     * @memberof Api
     */
    static post (url, params, headers = {}) {
        return request
            .post(url)
            .set(Api.headers(headers))
            .send(params);
    }
}
