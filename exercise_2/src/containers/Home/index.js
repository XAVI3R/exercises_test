import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Alert from '../../components/Alert';
import ListPosts from '../../components/ListPosts';
import Form from '../../components/Form';
import Loader from '../../components/Loader';
import { fetchPosts, submitPost } from '../../store/actions/posts';

/**
 * Generate props from redux state.
 *
 * @param {object} state
 */
const mapStateToProps = state => {
    return {
        listPosts: state.posts.listPosts,
        fetchingPosts: state.posts.fetchingPosts,
        errorFetchingPosts: state.posts.errorFetchingPosts,
        errorSavingPost: state.posts.errorSavingPost
    };
};

/**
* Generate props from redux actions.
*
* @param {Function} dispatch
*/
const mapDispatchtoProps = dispatch => {
    return {
        fetchPosts: () => {
            dispatch(fetchPosts());
        },
        submitPost: (newPost) => {
            dispatch(submitPost(newPost));
        }
    };
};

/**
 * Home of the application.
 *
 * @class Home
 * @extends {Component}
 */
class Home extends Component {
    /**
     * Props validation for the Home component.
     *
     * @static
     * @memberof Home
     */
    static propTypes = {
        listPosts: PropTypes.array.isRequired,
        fetchingPosts: PropTypes.bool.isRequired,
        errorFetchingPosts: PropTypes.array,
        errorSavingPost: PropTypes.array,
        fetchPosts: PropTypes.func.isRequired,
        submitPost: PropTypes.func.isRequired
    }

    /**
     * Creates an instance of the Home component.
     *
     * @param {object} props
     * @memberof Home
     */
    constructor (props) {
        super(props);

        this.state = {
            fetchingPosts: true
        };
    }

    /**
     * Call WS to get posts on component did mount.
     *
     * @memberof Home
     */
    componentDidMount () {
        this.fetchPosts();
    }

    /**
     * Change fetching status if the WS call is done.
     *
     * @param {object} props
     * @param {object} state
     * @memberof Home
     */
    static getDerivedStateFromProps (props, state) {
        if (state.fetchingPosts && !props.fetchingPosts) {
            return { fetchingPosts: !state.fetchingPosts };
        }

        return null;
    }

    /**
     * Fetch data.
     *
     * @memberof Home
     */
    fetchPosts = () => {
        this.setState({
            fetchingPosts: true
        });

        this.props.fetchPosts();
    }

    /**
     * Submit a new post.
     *
     * @param {object} newPost
     * @memberof Home
     */
    submitPost = (newPost) => {
        this.props.submitPost(newPost);
    }

    /**
     * Get home content render if there isn't an error.
     *
     * @returns {ReactElement}
     * @memberof Home
     */
    getHomeContent = () => {
        return !this.state.fetchingPosts ?
            [
                <Form
                    key={0}
                    submitPost={this.submitPost}
                    listPosts={this.props.listPosts}
                    fetchingPosts={this.props.fetchingPosts}
                    errorSavingPost={this.props.errorSavingPost} />,
                <ListPosts
                    key={1}
                    listPosts={this.props.listPosts.sort((postA, postB) => postB.id - postA.id)}
                    showFormPost={this.showFormPost} />

            ] :
            <Loader classes="loaderCenter" />;
    }

    /**
     * Renders the Home component.
     *
     * @returns {ReactElement}
     * @memberof Home
     */
    render () {
        return (
            <div className="container">
                {
                    this.props.errorFetchingPosts &&
                        <Alert
                            classes="mt-5 alert-danger"
                            text={this.props.errorFetchingPosts}
                            buttonAction={this.fetchPosts}
                            buttonText="Réessayer" />
                }

                {
                    !this.props.errorFetchingPosts &&
                        this.getHomeContent()
                }
            </div>
        );
    }
}

export default connect(
    mapStateToProps,
    mapDispatchtoProps
)(Home);
