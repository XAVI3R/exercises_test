import initialState from './initialState';
import * as types from '../actions/actionsTypes';

/**
 * Posts reducer.
 *
 * @export
 * @param {object} state
 * @param {object} action
 */
export default function posts (state = initialState.posts, action) {
    switch (action.type) {
        case types.FETCHING_POSTS:
            return {
                ...state,
                fetchingPosts: true,
                errorFetchingPosts: null,
                errorSavingPosts: null
            };
        case types.ERROR_FETCHING_POSTS:
            return {
                ...state,
                fetchingPosts: false,
                errorFetchingPosts: action.errorFetchingPosts
            };
        case types.ERROR_SAVING_POST:
            return {
                ...state,
                fetchingPosts: false,
                errorSavingPost: action.errorSavingPost
            };
        case types.SET_POSTS:
            return {
                fetchingPosts: false,
                listPosts: action.listPosts
            };
        default:
            return state;
    }
}
