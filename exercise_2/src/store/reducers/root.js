import { combineReducers } from 'redux';
import posts from './posts';

// Combine all reducers
const rootReducer = combineReducers({
    posts
});

export default rootReducer;
