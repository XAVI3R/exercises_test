// Initial state of redux store
export default {
    posts: {
        listPosts: [],
        fetchingPosts: false,
        errorFetchingPosts: null,
        errorSavingPost: null
    }
};
