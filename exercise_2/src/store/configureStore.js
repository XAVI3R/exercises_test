/* global window */
import { createStore, applyMiddleware } from 'redux';
import rootReducer from './reducers/root';
import thunk from 'redux-thunk';

/**
 * Create the redux store.
 *
 * @export
 * @return
 */
export default function configureStore () {
    return createStore(
        rootReducer,
        window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
        applyMiddleware(thunk)
    );
}
