import * as types from './actionsTypes';
import { getPosts, savePost } from '../../services/Post';

/**
 * Start fetching posts or saving post.
 */
export const fetchingPosts = () => {
    return {
        type: types.FETCHING_POSTS
    };
};

/**
 * Handle error fetching posts.
 *
 * @param {string} errorFetchingPosts
 */
export const errorFetchingPosts = errorFetchingPosts => {
    return {
        type: types.ERROR_FETCHING_POSTS,
        errorFetchingPosts
    };
};

/**
 * Handle error saving post.
 *
 * @param {string} errorSavingPost
 */
export const errorSavingPost = errorSavingPost => {
    return {
        type: types.ERROR_SAVING_POST,
        errorSavingPost
    };
};

/**
 * Set posts list.
 *
 * @param {array} listPosts
 */
export const setPosts = listPosts => {
    return {
        type: types.SET_POSTS,
        listPosts
    };
};

/**
 * Handle the fetch of posts.
 */
export const fetchPosts = () => (dispatch) => {
    dispatch(fetchingPosts());

    getPosts()
        .then(response => {
            dispatch(setPosts(response.body));
        })
        .catch(error => {
            dispatch(errorFetchingPosts(error.response.body.content));
        });
};

/**
 * Handle the submit of a new post.
 *
 * @param {object} newPost
 */
export const submitPost = (newPost) => (dispatch, getState) => {
    dispatch(fetchingPosts());

    savePost(newPost)
        .then(response => {
            const state = getState();
            let listPosts = state.posts.listPosts;
            listPosts.push(response.body);
            dispatch(setPosts(listPosts));
        })
        .catch(error => {
            dispatch(errorSavingPost(error.response.body.content));
        });
};
