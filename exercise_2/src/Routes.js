import React from 'react';
import Home from './containers/Home';
import Header from './components/Header';
import { Route, Switch, BrowserRouter } from 'react-router-dom';

export default (props) => (
    <BrowserRouter>
        <div>
            <Header />
            <Switch>
                <Route exact path="/" component={Home} />
            </Switch>
        </div>
    </BrowserRouter>
);

