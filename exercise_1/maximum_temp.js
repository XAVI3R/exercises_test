function maximum_tem(tempList) {
    if (tempList.length < 3) {
        return 0;
    }

    let min = tempList[0] < tempList[1] ? tempList[0] : tempList[1];
    let max = tempList[1];
    let maxDiff = max - min;

    for (i = 1; i < tempList.length; i++) {
        let temp = tempList[i];

        if (temp < max ) {
            if (max - min > maxDiff) {
                maxDiff = max - min;
            }
            
            min = temp;
        }

        max = temp;
    }

    return Math.max(maxDiff, max - min);
}

console.log("[10, 20, 30, 40, 50] => " + maximum_tem([10, 20, 30, 40, 50]));
console.log("[10, 20, 9, 16, 17] => " + maximum_tem([10, 20, 9, 16, 17]));
console.log("[5, 4, 3] => " + maximum_tem([5, 4, 3]));
console.log("[1] => " + maximum_tem([1]));
console.log("[5, 2, 1, 4, 9, 2, 15] => " + maximum_tem([5, 2, 1, 4, 9, 2, 15]));
console.log('[5, 3, 1, 8, 9, 6, 20, 21, 34, 20, 15, 14, 17, 16, 16, 10, 5, 6, 8, 10, 11, 20, 22, 24, 29, 30, 34, 28, 20, 12, 8, 15, 14, 10, 36, 20] => ' + maximum_tem([5, 3, 1, 8, 9, 6, 20, 21, 34, 20, 15, 14, 17, 16, 16, 10, 5, 6, 8, 10, 11, 20, 22, 24, 29, 30, 34, 28, 20, 12, 8, 15, 14, 10, 36, 20]));
