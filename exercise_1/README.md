## Exercice 1

Simple Javascript function that return the maximum temperature increase between consecutive temperatures from a list of temperatures.

### Run

Paste the function in a JavaScript editor or you can find the results here : http://jsbin.com/wofiqoqewu/edit?js,console